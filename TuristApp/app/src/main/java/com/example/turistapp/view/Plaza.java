package com.example.turistapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.turistapp.R;
import com.example.turistapp.controller.rutaPla;

public class Plaza extends AppCompatActivity {

    Button ruta;
    Button inicio;TextView textEnlace;

    String url ="https://es.wikipedia.org/wiki/Plaza_de_Armas_de_Arequipa";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plaza);

        ruta=(Button)findViewById(R.id.btnRuta);
        inicio=(Button)findViewById(R.id.btnInit);

        ruta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent rutaplaza= new Intent(Plaza.this, rutaPla.class);
                startActivity(rutaplaza);
                finish();

            }
        });

        inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent volver= new Intent(Plaza.this, lugares.class);
                startActivity(volver);
                finish();

            }
        });

    }


}
