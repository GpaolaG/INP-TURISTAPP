package com.example.turistapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

import com.example.turistapp.R;

public class lugares extends AppCompatActivity {

    ImageButton plaza;
    ImageButton lazaro;
    ImageButton catalina;
    ImageButton iglesia;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lugares);


        plaza=(ImageButton) findViewById(R.id.btnPlaza);
        lazaro=(ImageButton) findViewById(R.id.btnLazaro);
        catalina=(ImageButton) findViewById(R.id.btnCatalina);
        iglesia=(ImageButton) findViewById(R.id.btnIglesia);


        plaza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent plaza=new Intent(lugares.this, Plaza.class);
                startActivity(plaza);

            }
        });

        lazaro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent lazaro=new Intent(lugares.this, Sanlazaro.class);
                startActivity(lazaro);

            }
        });


        catalina.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent catalina=new Intent(lugares.this, Catalina.class);
                startActivity(catalina);

            }
        });
        iglesia.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent catalina=new Intent(lugares.this, iglesia.class);
                startActivity(catalina);

            }
        });





    }
}
