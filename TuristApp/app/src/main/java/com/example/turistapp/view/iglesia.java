package com.example.turistapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.turistapp.R;
import com.example.turistapp.controller.rutaIglesia;

public class iglesia extends AppCompatActivity {
    Button ruta;
    Button inicio;
    TextView textEnlace;

    String url ="https://es.wikipedia.org/wiki/Iglesia_de_la_Compa%C3%B1%C3%ADa_(Arequipa)";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_iglesia);

        ruta=(Button)findViewById(R.id.btnRuta);
        inicio=(Button)findViewById(R.id.btnInit);


        ruta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent rutaiglesia= new Intent(iglesia.this,rutaIglesia.class);
                startActivity(rutaiglesia);
                finish();

            }
        });

        inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent volver= new Intent(iglesia.this, lugares.class);
                startActivity(volver);
                finish();

            }
        });

    }
}
