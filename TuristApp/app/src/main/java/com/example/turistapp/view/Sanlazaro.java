package com.example.turistapp.view;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.turistapp.R;
import com.example.turistapp.controller.rutaLaza;

public class Sanlazaro extends AppCompatActivity {

    Button ruta;
    Button inicio;
    TextView textEnlace;

    String url ="http://rpp.pe/peru/actualidad/el-barrio-de-san-lazaro-el-mas-antiguo-de-arequipa-noticia-394013";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sanlazaro);

        ruta=(Button)findViewById(R.id.btnRuta);
        inicio=(Button)findViewById(R.id.btnInit);

        ruta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent rutaLazaro= new Intent(Sanlazaro.this, rutaLaza.class);
                startActivity(rutaLazaro);
                finish();

            }
        });

        inicio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent volver= new Intent(Sanlazaro.this, lugares.class);
                startActivity(volver);
                finish();

            }
        });

    }
}
