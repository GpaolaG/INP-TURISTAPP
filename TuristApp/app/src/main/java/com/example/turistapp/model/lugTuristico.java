package com.example.turistapp.model;

import android.media.Image;

import java.io.Serializable;

/**
 * Created by Gleny on 04/12/2017.
 */

public class lugTuristico {

    public int getId_Lugar() {
        return id_Lugar;
    }

    public void setId_Lugar(int id_Lugar) {
        this.id_Lugar = id_Lugar;
    }

    public int getId_Usuario() {
        return id_Usuario;
    }

    public void setId_Usuario(int id_Usuario) {
        this.id_Usuario = id_Usuario;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getInfo() {
        return Info;
    }

    public void setInfo(String info) {
        Info = info;
    }

    int id_Lugar;
    int id_Usuario;
    Image image;
    String Info;
}
