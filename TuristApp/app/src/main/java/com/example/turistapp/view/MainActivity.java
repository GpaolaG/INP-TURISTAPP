
package com.example.turistapp.view;

        import android.content.Intent;
        import android.support.v7.app.AppCompatActivity;
        import android.os.Bundle;
        import android.view.View;
        import android.widget.Button;

        import com.example.turistapp.R;
        import com.example.turistapp.controller.Login;

public class MainActivity extends AppCompatActivity {

    Button btnRegistrar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        btnRegistrar = (Button) findViewById(R.id.btnConectar);

        btnRegistrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent login= new Intent(MainActivity.this, Login.class);
                startActivity(login);
                finish();

            }
        });
    }


}




